(function () {
    'use strict';

    angular
        .module('myApp')
        .service('categoryService', categoryService);

    categoryService.$inject = ['$http'];
    function categoryService($http) {
        var service = {};
        //API Endpoint
        var bannerdetailsUrl="http://localhost:8080/ProgramBannerTemplate/api/v1.0/banner/banner";
        var insertBanner="http://localhost:8080/ProgramBannerTemplate/api/v1.0/banner/insertbanner";
        var bannerTemplates="http://localhost:8080/ProgramBannerTemplate/api/v1.0/banner/loadall";
        	
        this.add = function(category) {
            return $http.post(api_url+'?type=addCategory', 
                category).then(handleSuccess, handleError('Error creating user'));
        }
        this.getAll = function() {
            return $http({
                url: bannerdetailsUrl, 
                method: "GET",
             }).then(handleSuccess,
               handleError('Error creating user'));
        }
        this.getTemplates = function() {
            return $http({
                url: bannerTemplates, 
                method: "GET",
             }).then(handleSuccess,
               handleError('Error creating user'));
        }
        this.postTemplates = function(bannerDetails) {
            return $http({
                url: insertBanner, 
                method: "POST",               
                params: {
                	'network_code':'DR',
                	'program_code':'Ja',
                	'region_code':'IN',
                	'template_id':2
                        }
             }).then(handleSuccess,
               handleError('Error creating user'));
        }
    
        // private functions

        function handleSuccess(res) {
            return  {data:res.data, success:true,message:''};
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
