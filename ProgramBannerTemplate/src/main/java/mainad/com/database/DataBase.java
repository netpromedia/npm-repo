package mainad.com.database;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import mainad.com.bannerdto.*;

public class DataBase {
	Configuration configuration;
	Session session;
	SessionFactory sessionFactory;
	Transaction transaction;

	public DataBase() {
		configuration = new Configuration();
		configuration.configure("bannerhibernate.cfg.xml");
		sessionFactory = configuration.buildSessionFactory();
		session = sessionFactory.openSession();
	}

	public List<Program_Master_Dto> programDetails() {
		String hql = "FROM Program_Master_Dto";
		Query qry = session.createQuery(hql);
		List<Program_Master_Dto> programresult = qry.list();
		return programresult;
	}

	public List<Network_Master_Dto> networkDetails() {
		String hql = "FROM Network_Master_Dto";
		Query qry = session.createQuery(hql);
		List<Network_Master_Dto> networkresult = qry.list();
		return networkresult;
	}

	public List<Region_Master_Dto> regionDetails() {
		String hql = "FROM Region_Master_Dto";
		Query qry = session.createQuery(hql);
		List<Region_Master_Dto> regionresult = qry.list();
		return regionresult;
	}

	public List<Template_Master_Dto> templateDetails() {
		String hql = "FROM Template_Master_Dto";
		Query qry = session.createQuery(hql);
		List<Template_Master_Dto> templateresult = qry.list();
		return templateresult;
	}
	
	public List<Banner_Template_Dto> bannerDetails() {
		String hql = "FROM Banner_Template_Dto";
		Query qry = session.createQuery(hql);
		List<Banner_Template_Dto> bannerresult = qry.list();
		return bannerresult;
	}

	public int saveTemplate(Template_Master_Dto t) {
		transaction = session.beginTransaction();
		Integer savedVAL = (Integer) session.save(t);
		if (null == savedVAL) {
			System.out.println("877");
		}
		transaction.commit();
		return savedVAL;
	}

	public int saveBannerTemplate(Banner_Template_Dto b) {
		transaction = session.beginTransaction();
		Integer savedVAL = (Integer) session.save(b);
		if (null == savedVAL) {
			System.out.println("877");
		}
		transaction.commit();
		return savedVAL;
	}

	@SuppressWarnings("deprecation")
	public int updateBanner(Banner_Template_Dto banner_Template_Dto){
		transaction = session.beginTransaction();
		Query query = session
                .createQuery("update Banner_Template_Dto set mtemp_id= :temp_id where mbanner_id= :banner_id");
        query.setInteger("temp_id", banner_Template_Dto.getMtemp_id());
        query.setLong("banner_id", banner_Template_Dto.getMbanner_id());
        int result = query.executeUpdate();
		transaction.commit();
		
		return result;
	}
}
