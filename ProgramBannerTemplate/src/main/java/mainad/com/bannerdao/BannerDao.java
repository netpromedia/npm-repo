package mainad.com.bannerdao;

import java.util.ArrayList;
import java.util.List;

import mainad.com.bannerdto.*;
import mainad.com.database.*;

public class BannerDao {

	public int insertTemplate(String template) {
		DataBase dataBase = new DataBase();
		Template_Master_Dto template_Master_Dto = new Template_Master_Dto();
		int mid = 0;
		try {
			template_Master_Dto.setMtemplate(template);
			mid = dataBase.saveTemplate(template_Master_Dto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mid;
	}

	public int insertBannerTemplate(String network_code, String program_code, String region_code, int template_id) {
		DataBase dataBase = new DataBase();
		Banner_Template_Dto banner_Template_Dto = new Banner_Template_Dto();
		int mid = 0;
		int tempid = 0;
		try {
			//tempid = Integer.parseInt(template_id);
			tempid=template_id;
			banner_Template_Dto.setMnetwork_code(network_code);
			banner_Template_Dto.setMprogram_code(program_code);
			banner_Template_Dto.setMregion_code(region_code);
			banner_Template_Dto.setMtemp_id(tempid);
			mid = dataBase.saveBannerTemplate(banner_Template_Dto);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mid;
	}

	public int updateBannerTemplate(int banner_id, String program_code, String region_code, String network_code,
			int temp_id) {
		DataBase dataBase = new DataBase();
		Banner_Template_Dto banner_Template_Dto = new Banner_Template_Dto();
		int status=0;
		try {
			banner_Template_Dto.setMtemp_id(temp_id);
			banner_Template_Dto.setMprogram_code(program_code);
			banner_Template_Dto.setMregion_code(region_code);
			banner_Template_Dto.setMnetwork_code(network_code);
			banner_Template_Dto.setMbanner_id(banner_id);
			status=dataBase.updateBanner(banner_Template_Dto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public BindingClass getAllDetails() {

		DataBase dataBase = null;
		BindingClass bindingClass = null;
		try {
			dataBase = new DataBase();
			bindingClass = new BindingClass();

			bindingClass.setProgram_Master_Dto(dataBase.programDetails());
			bindingClass.setRegion_Master_Dto(dataBase.regionDetails());
			bindingClass.setTemplate_Master_Dto(dataBase.templateDetails());
			bindingClass.setNetwork_Master_Dto(dataBase.networkDetails());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bindingClass;
	}

	public List<Banner_Template_Dto> getAllBannerDetails() {
		DataBase dataBase = null;
		List<Banner_Template_Dto> bannerlist = null;
		try {
			dataBase = new DataBase();
			bannerlist = dataBase.bannerDetails();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bannerlist;
	}
}
