package mainad.com.webservice;

import java.util.List;

import javax.ws.rs.Consumes;
//import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.w3c.tidy.Out;

import mainad.com.bannerdao.*;
import mainad.com.bannerdto.Banner_Template_Dto;
import mainad.com.bannerdto.BindingClass;

@Path("/v1.0/banner")
public class Webservice {

	@GET
	@Path("/banner")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBannerTemplateDetails() {
		BannerDao bannerDao = new BannerDao();
		BindingClass bindingClass = null;
		try {

			bindingClass = bannerDao.getAllDetails();

		} catch (Exception e) {

		}

		return Response.ok(bindingClass).build();
	}

	@POST
	@Path("/insertbanner")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response insertBannerTemplate(@FormDataParam("network_code") String network_code,
			@FormDataParam("program_code") String program_code, @FormDataParam("region_code") String region_code,
			@FormDataParam("template_id") int template_id) {
		BannerDao bannerDao = new BannerDao();
		int id = 0;
		try {
			id = bannerDao.insertBannerTemplate(network_code, program_code, region_code, template_id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok(id).build();
	}

	@POST
	@Path("/inserttemplate")
	public Response insertTemplate(@FormParam("template") String template) {
		BannerDao bannerDao = new BannerDao();
		int id = 0;
		try {
			id = bannerDao.insertTemplate(template);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok(id).build();
	}

	@GET
	@Path("/loadall")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadAllBannerDetails() {
		BannerDao bannerDao = new BannerDao();
		List<Banner_Template_Dto> bannerdetails = null;
		try {
			bannerdetails = bannerDao.getAllBannerDetails();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok(bannerdetails).build();
	}

	
	@PUT
	@Path("/updatebanner")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateBannerTemplate(@FormDataParam("banner_id") int banner_id,
			@FormDataParam("network_code") String network_code, @FormDataParam("program_code") String program_code,
			@FormDataParam("region_code") String region_code, @FormDataParam("template_id") int template_id) {
		BannerDao bannerDao = new BannerDao();
		int status=0;
		try {
			status=bannerDao.updateBannerTemplate(banner_id, network_code, program_code, region_code, template_id);
			
		} catch (Exception e) {
			return Response.ok(e.getMessage()).build();
			//e.printStackTrace();
		}
		if(0 < status)
		{
			return Response.ok("updated successfully").build();
		}
		else
		{
		return Response.ok("Sorry Check the details you have submitted").build();
		}
	}
}
