package mainad.com.bannerdto;

import java.util.List;

public class BindingClass {

	List<Program_Master_Dto> program_Master_Dto;
	List<Region_Master_Dto> region_Master_Dto;
	List<Network_Master_Dto> network_Master_Dto;
	List<Template_Master_Dto> template_Master_Dto;

	public List<Program_Master_Dto> getProgram_Master_Dto() {
		return program_Master_Dto;
	}

	public void setProgram_Master_Dto(List<Program_Master_Dto> program_Master_Dto) {
		this.program_Master_Dto = program_Master_Dto;
	}

	public List<Region_Master_Dto> getRegion_Master_Dto() {
		return region_Master_Dto;
	}

	public void setRegion_Master_Dto(List<Region_Master_Dto> region_Master_Dto) {
		this.region_Master_Dto = region_Master_Dto;
	}

	public List<Network_Master_Dto> getNetwork_Master_Dto() {
		return network_Master_Dto;
	}

	public void setNetwork_Master_Dto(List<Network_Master_Dto> network_Master_Dto) {
		this.network_Master_Dto = network_Master_Dto;
	}

	public List<Template_Master_Dto> getTemplate_Master_Dto() {
		return template_Master_Dto;
	}

	public void setTemplate_Master_Dto(List<Template_Master_Dto> template_Master_Dto) {
		this.template_Master_Dto = template_Master_Dto;
	}
}
