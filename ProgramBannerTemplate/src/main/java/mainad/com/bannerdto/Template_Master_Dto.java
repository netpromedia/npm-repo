package mainad.com.bannerdto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="template_master")
public class Template_Master_Dto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="temp_id")
	private int mtemplate_id;
	private String mtemplate;

	public int getMtemplate_id() {
		return mtemplate_id;
	}

	public void setMtemplate_id(int mtemplate_id) {
		this.mtemplate_id = mtemplate_id;
	}

	public String getMtemplate() {
		return mtemplate;
	}

	public void setMtemplate(String mtemplate) {
		this.mtemplate = mtemplate;
	}

}
