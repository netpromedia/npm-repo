package mainad.com.bannerdto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "banner_template")
public class Banner_Template_Dto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "banner_id")
	private int mbanner_id;

	private String mprogram_code;
	private String mregion_code;
	private String mnetwork_code;
	private int mtemp_id;

	public String getMprogram_code() {
		return mprogram_code;
	}

	public void setMprogram_code(String mprogram_code) {
		this.mprogram_code = mprogram_code;
	}

	public String getMregion_code() {
		return mregion_code;
	}

	public void setMregion_code(String mregion_code) {
		this.mregion_code = mregion_code;
	}

	public String getMnetwork_code() {
		return mnetwork_code;
	}

	public void setMnetwork_code(String mnetwork_code) {
		this.mnetwork_code = mnetwork_code;
	}

	public int getMtemp_id() {
		return mtemp_id;
	}

	public void setMtemp_id(int mtemp_id) {
		this.mtemp_id = mtemp_id;
	}

	public int getMbanner_id() {
		return mbanner_id;
	}

	public void setMbanner_id(int mbanner_id) {
		this.mbanner_id = mbanner_id;
	}

}
