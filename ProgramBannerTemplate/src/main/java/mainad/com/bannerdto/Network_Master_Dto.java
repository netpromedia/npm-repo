package mainad.com.bannerdto;

public class Network_Master_Dto {
	private String mnetwork_name;
	private String mnetwork_code;

	public String getMnetwork_name() {
		return mnetwork_name;
	}

	public void setMnetwork_name(String mnetwork_name) {
		this.mnetwork_name = mnetwork_name;
	}

	public String getMnetwork_code() {
		return mnetwork_code;
	}

	public void setMnetwork_code(String mnetwork_code) {
		this.mnetwork_code = mnetwork_code;
	}
}
