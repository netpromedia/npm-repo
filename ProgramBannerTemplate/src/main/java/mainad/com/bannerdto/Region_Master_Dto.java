package mainad.com.bannerdto;

public class Region_Master_Dto {
	private String mregion_name;
	private String mregion_code;

	public String getMregion_name() {
		return mregion_name;
	}

	public void setMregion_name(String mregion_name) {
		this.mregion_name = mregion_name;
	}

	public String getMregion_code() {
		return mregion_code;
	}

	public void setMregion_code(String mregion_code) {
		this.mregion_code = mregion_code;
	}
}
