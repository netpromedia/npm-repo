package mainad.com.bannerdto;

public class Program_Master_Dto {
	private String mprogramname;
	private String mprogram_code;

	public String getMprogramname() {
		return mprogramname;
	}

	public void setMprogramname(String mprogramname) {
		this.mprogramname = mprogramname;
	}

	public String getMprogram_code() {
		return mprogram_code;
	}

	public void setMprogram_code(String mprogram_code) {
		this.mprogram_code = mprogram_code;
	}
}
